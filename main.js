const express = require('express');
const app = express();
const PORT = process.env.PORT || 5000;
const exphbs = require('express-handlebars');
const session = require('express-session');
const fileupload= require('express-fileupload');
const bodyParser = require('body-parser');
const category = require('./controllers/categoryC');
const product = require('./controllers/productsC');
const account = require('./controllers/accountC');
const home = require('./controllers/homeC');
const bid=require('./controllers/bidC');
const hbs = exphbs.create({
    defaultLayout: 'main.hbs',
    extname: 'hbs',
    helpers:{
        add: function(v1,v2){return parseInt(v1)+parseInt(v2);
        },
        section: function(name, options) { 
            if (!this._sections) this._sections = {};
              this._sections[name] = options.fn(this); 
              return null;
            },
        if_eq: function(arg1, arg2, options) {
            return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
        }
    }
});

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));
app.use(fileupload());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
    secret: '123456',
    resave: false,
    saveUninitialized: false,
}));
app.use('/', home);
app.use('/', category);
app.use('/', product);
app.use('/', account);
app.use('/', bid);

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));