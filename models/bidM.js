const db = require('../utils/db');
const mysql = require('mysql');
const tbName = 'bid_history';

module.exports = {
    add: async bid => {
        const id = await db.add(tbName, bid);
        return id;
    },
    del: async (pro,user)=>{
        const sql= `DELETE FROM ${tbName} WHERE ProID=${pro} AND UserID='${user}'`;
        const id= await db.load(sql);
        return id;
    },
    topByProID: async(id)=>{
        const sql=`SELECT * FROM ${tbName} JOIN users ON ${tbName}.UserID=users.f_ID WHERE ProId='${id}' ORDER BY Price DESC, DateBegin DESC LIMIT 1`;
        const rows= await db.load(sql);
        if (rows.length>0) return rows[0];
        return null;
    },
    allByProID: async(id)=>{
        const sql=`SELECT * FROM ${tbName} JOIN users ON ${tbName}.UserID=users.f_ID WHERE ProId='${id}' ORDER BY Price DESC, DateBegin DESC;`;
        const rows= await db.load(sql);
        return rows;
    },
    allByUserID: async(id)=>{
        const sql=`SELECT distinct products.ProID,SelID,ProName, CurrentPrice,DateEnd,products.DateBegin,UserID FROM products join ${tbName} on products.ProID=${tbName}.ProID
        WHERE ${tbName}.UserID='${id}' ORDER BY ${tbName}.DateBegin DESC;`;
        const rows =await db.load(sql)
            return rows;
    },
    ProIDByUserID: async id=>{
        const sql=`SELECT ProID FROM ${tbName} WHERE UserID='${id}'`;
        const rows =await db.load(sql)
  
            return rows;
    },
    isExist : async (pro,user)=>{
        const sql= `SELECT (count(*)>'0') AS exist FROM ${tbName} WHERE ProID='${pro}' AND UserID='${user}'`;
        const id= await db.load(sql);
        return id[0];
    }
};