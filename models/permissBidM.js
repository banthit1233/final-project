const db = require('../utils/db');
const mysql = require('mysql');
const tbName = 'PermissToBid';

module.exports = {
    add: async permiss => {
        const id = await db.add(tbName, permiss);
        return id;
    },
    addTosell: async permiss => {
        const id = await db.add('PermissToSell', permiss);
        return id;
    },
    delTosell:async(id)=>{
        const sql=`DELETE FROM PermissToSell WHERE PermissID=${id}`;
        const did= await db.load(sql);
        return did;
    },
    del: async (id)=>{
        const sql= `DELETE FROM ${tbName} WHERE PermissID='${id}'`;
        const did= await db.load(sql);
        return did;
    },
    update:async(per)=>{
        const uid= await db.update(tbName,'PermissID',per);
        return uid;
    },
    allPerBySellID:async id=>{
        const sql=`SELECT distinct PermissID,BidderID,${tbName}.Date,f_Username,${tbName}.ProID,ProName
        FROM ${tbName} join products on products.ProID=${tbName}.ProID 
        join users on users.f_ID=${tbName}.BidderID
         WHERE ${tbName}.SellerID='${id}' AND  ${tbName}.Status='0' 
         ORDER BY ${tbName}.Date DESC;`;
        const rows= await db.load(sql);
        return rows;
    },
    isExistSell:async (Bid)=>{
        const sql=`SELECT count(distinct BidderID) AS result FROM PermissToSell 
        WHERE PermissToSell.BidderID=${Bid};`;
        const result =await db.load(sql);
        if (result[0].result==0)
            return false;
        return true;
    },
    isExist:async (Pro,Bid,Sel)=>{
        const sql=`SELECT count(distinct ProID,BidderID,SellerID) AS result FROM ${tbName} 
        WHERE ProID=${Pro} AND BidderID=${Bid} AND SellerID=${Sel} AND ${tbName}.Status='0' ;`;
        const result =await db.load(sql);
        if (result[0].result==0)
            return false;
        return true;
    },
    getPermissByBidID:async(Pro,Bid)=>{
        const sql=`SELECT Status FROM ${tbName} WHERE ProID='${Pro}' AND BidderID='${Bid}'`
        const result=await db.load(sql);
        if (result.length>0)
            return result[0].Status;
        return 0;
    }
};