const db = require('../utils/db');
const mysql = require('mysql');
const tbName = 'rating';

module.exports = {
    add: async rate => {
        const id = await db.add(tbName, rate);
        return id;
    },
    allByReceiveID: async id=>{
        const sql=`SELECT * FROM ${tbName} join users on users.f_ID=${tbName}.sendID
        where rating.ReceiveID='${id}';`;
        const rows =await db.load(sql);
            return rows;
    },
    allBySendID: async id=>{
        const sql=`SELECT * FROM ${tbName} WHERE RateID SendID='${id}'`;
        const rows =await db.load(sql);
            return rows;
    },
    RateByReceiveID: async id=>{
        const sql=`SELECT (count(*)/total.t) AS result FROM ${tbName} , (SELECT count(*) AS t FROM ${tbName}   WHERE ReceiveID='${id}') AS total WHERE ReceiveID='${id}' AND Rate>0;`;
        const rows =await db.load(sql);
        const res=rows[0].result;
            if (!res || res<0)
                return 0;
            else return Math.floor(res*100);
    },
};