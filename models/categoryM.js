const db = require('../utils/db');
const tbName = 'categories';
module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        if (rows.length>0){
            return rows;
        }
        return null;
    },
    CatBySubCatID:async(id)=>{
        const sql= `SELECT * FROM sub_categories WHERE SubCatID=${id}`;
        const rows = await db.load(sql);
        if (rows.length>0){
            return rows[0].CatID;
        }
        return null;
    },
    allSubCatByID: async (id)=>{
        const sql= `SELECT * FROM sub_categories WHERE CatID=${id}`;
        const rows = await db.load(sql);
        if (rows.length>0){
            return rows;
        }
        return null;
    },
}