const db = require('../utils/db');
const mysql = require('mysql');
const tbName = 'favorite';

module.exports = {
    add: async fav => {
        const id = await db.add(tbName, fav);
        return id;
    },
    del: async (pro,user)=>{
        const sql= `DELETE FROM ${tbName} WHERE ProID=${pro} AND UserID='${user}'`;
        const id= await db.load(sql);
        return id;
    },
    
    ProIDByUserID: async id=>{
        const sql=`SELECT ProID FROM ${tbName} WHERE UserID='${id}'`;
        const rows =await db.load(sql)
  
            return rows;
    },
    allProByUserID:async id=>{
        const sql=`SELECT * FROM ${tbName} join products on products.ProID=${tbName}.ProID WHERE ${tbName}.UserID='${id}';`;
        const rows= await db.load(sql);
        return rows;
    },
    isExist : async (pro,user)=>{
        const sql= `SELECT (count(*)>'0') AS exist FROM ${tbName} WHERE ProID='${pro}' AND UserID='${user}'`;
        const id= await db.load(sql);
        return id[0];
    }
};