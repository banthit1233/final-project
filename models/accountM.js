const db = require('../utils/db');
const mysql = require('mysql');
const tbName = 'users';

module.exports = {
    add: async user => {
        const id = await db.add(tbName, user);
        return id;
    },
    getByPermiss:async ()=>{
        const sql=`SELECT * FROM ${tbName} join PermissToSell on PermissToSell.BidderID=${tbName}.f_ID WHERE ${tbName}.f_Permission='2';`;
        const rs = await db.load(sql);
            return rs;
    },
    
    getByUsername: async username=>{
        let sql = `SELECT * FROM ${tbName} WHERE f_Username = '${username}'`;
        const rs = await db.load(sql);
        if (rs.length>0){
            return rs[0];
        }
        return null;
    },
    getByID: async id=>{
        let sql = `SELECT * FROM ${tbName} WHERE f_ID = '${id}'`;
        const rs = await db.load(sql);
        if (rs.length>0){
            return rs[0];
        }
        return null;
    },
    getByEmail: async(email)=>{
        let sql = `SELECT * FROM ${tbName} WHERE f_Email = '${email}'`;
        const rs = await db.load(sql);
        if (rs.length>0){
            return rs[0];
        }
        return null;
    },
    update:async(user)=>{
        const uid = await db.update(tbName,'f_ID',user);
        return uid;
    },
};