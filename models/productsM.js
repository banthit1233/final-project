const db = require('../utils/db');
const tbName = 'products';
const pageSize = 4;
module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        if (rows.length>0){
            return rows;
        }
        return null;
    },
    add: async pro => {
        const id = await db.add(tbName, pro);
        return id;
    },
    allByOrder: async(keyString, des)=>{
        var keyOrder='DESC';
        if (des==false)
            keyOrder='';
        var sql = `SELECT * FROM ${tbName} ORDER BY ${keyString} ${keyOrder} LIMIT 5;`;
        if (keyString=='TimeLeft')
            sql=`SELECT * FROM products ORDER BY DateEnd-current_timestamp() ${keyOrder} LIMIT 5;`;
        const rows = await db.load(sql);
        if (rows.length>0){
            return rows;
        }
        return null;
    },
    allByProID: async(id)=>{
        const sql=`SELECT * FROM ${tbName} WHERE ProID=${id}`;
        const rows= await db.load(sql);
        if (rows.length>0){
            return rows;
        }
        return null;
    },
    allByCatId: async (id) => {
        const sql = `SELECT * FROM ${tbName} WHERE CatID=${id}`;
        const rows = await db.load(sql);
            return rows;
       
    },
    allBySelID: async(id)=>{
        const sql = `SELECT * FROM ${tbName} WHERE SelID=${id}`;
        const rows = await db.load(sql);
            return rows;
    },
    allSelBySelID: async(id)=>{
        const sql = `SELECT * FROM ${tbName} WHERE SelID=${id} AND Amount>0`;
        const rows = await db.load(sql);
            return rows;
 
    },
    allByCatIdPaging: async (id, page) => {
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.ceil(totalP / pageSize);
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} LIMIT ${pageSize} OFFSET ${offset}`;
        const rows=await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        }
    },
    allBySubCatIdPaging: async (id, page) => {
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE SubCatID=${id}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.ceil(totalP / pageSize);
        const offset = (page - 1) * pageSize;
        sql = `SELECT * FROM ${tbName} WHERE SubCatID=${id} LIMIT ${pageSize} OFFSET ${offset}`;
        const rows=await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        }
    },
    update:async(pro)=>{
        const uid= await db.update(tbName,'ProID',pro);
        return uid;
    },
    
    search:async(colField,keyField,order,page)=>{
        let sql = `SELECT count(*)  AS total FROM ${tbName} WHERE MATCH(${colField}) AGAINST('${keyField}' IN NATURAL LANGUAGE MODE);`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.ceil(totalP / pageSize);
        const offset = (page - 1) * pageSize;
        var orderField="";
        if(order=='1') 
            orderField=`ORDER BY ${tbName}.CurrentPrice `;
        else
         if(order=='2')
            orderField=`ORDER BY ${tbName}.DateEnd DESC`;
        sql = `SELECT * FROM ${tbName} WHERE MATCH(${colField}) AGAINST('${keyField}' IN NATURAL LANGUAGE MODE) ${orderField} LIMIT ${pageSize} OFFSET ${offset}`;
        const rows=await db.load(sql);
        
        return {
            pageTotal: pageTotal,
            products: rows
        }
    },
    searchByCat:async(colField,keyField,order,page)=>{
        let sql = `SELECT count(*) AS total FROM ${tbName} JOIN (select * FROM categories WHERE MATCH(${colField})
        AGAINST('${keyField}' IN NATURAL LANGUAGE MODE)) AS result ON result.CatID=products.CatID;`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.ceil(totalP / pageSize);
        const offset = (page - 1) * pageSize;
        var orderField="";
        if(order=='1') 
            orderField=`ORDER BY ${tbName}.CurrentPrice `;
        else
         if(order=='2')
            orderField=`ORDER BY ${tbName}.DateEnd DESC`;
        sql = `SELECT * FROM ${tbName} JOIN (select * FROM categories WHERE MATCH(${colField})
         AGAINST('${keyField}' IN NATURAL LANGUAGE MODE)) AS result ON result.CatID=products.CatID ${orderField} LIMIT ${pageSize} OFFSET ${offset};`;
        const rows=await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        }
    },
    getID:async ()=>{
        const sql=`SELECT max(ProID) as result FROM ${tbName};`;
        const rows= await db.load(sql);
         return rows[0].result+1;
    }
};