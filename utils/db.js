const mysql = require('mysql');
function createConnection(){
    return mysql.createConnection({
        host:'localhost',
        user:'root',
        password:'1234',
        port:'3306',
        database:'qldg',
    });
}

exports.load = sql =>{
    return new Promise((resolve, reject)=>{
        const con=createConnection();
        con.connect(err=>{
            if (err)
                reject(err);
        });
        con.query(sql,(error,results,fields)=>{
            if (error)
                reject(error);
            resolve(results);
        });
        con.end();
    });
};
exports.add = (tbName, entity) =>{
    return new Promise((resole, reject) =>{
    const con = createConnection();
    con.connect(err =>{
        if(err) {
            reject(err);
        }
    });
    const sql = `INSERT INTO ${tbName} SET ?`;
    con.query(sql,entity, (error, results, fields) => {
        if (error) { 
            reject(error);
        }
        resole(results);
    });
    con.end();
    });
};
exports.update = (tbName, idField, entity) =>{
    return new Promise((resole, reject) =>{
    const con = createConnection();
    con.connect(err =>{
        if(err) {
            reject(err);
        }
    });
    const id =entity[idField];
    delete entity[idField];
    const sql = `UPDATE ${tbName} SET ? WHERE ${idField}=${id}`;
    con.query(sql,entity, (error, results, fields) => {
        if (error) { 
            reject(error);
        }
        resole(results);
    });
    con.end();
    });
};
exports.search = (tbName, colField,keyField) =>{
    return new Promise((resole, reject) =>{
    const con = createConnection();
    con.connect(err =>{
        if(err) {
            reject(err);
        }
    });
    const sql = `SELECT * FROM ${tbName} WHERE MATCH(${colField}) AGAINST('${keyField}' IN NATURAL LANGUAGE MODE);`;
    con.query(sql, (error, results, fields) => {
        if (error) { 
            reject(error);
        }
        resole(results);
    });
    con.end();
    });
};
