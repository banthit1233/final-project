const express = require('express');
const router = express.Router();
const mCat = require('../models/categoryM');
const mPro = require('../models/productsM');
const mAccount=require('../models/accountM');
const favoriteM= require('../models/favoriteM');
const mBid= require('../models/bidM');

function formatPrice(price){
    var result=" đ";
    while (price>0){
        let str=price.toString()
        let r= str.substring(str.length-3, str.length);
        result= ',' + r + result;
        price=Math.floor(price/1000);
    }
    return result.substring(1);
}
function CalTime(p) {
    var date = new Date(p.DateEnd - Date.now());
    
    var year = date.getUTCFullYear() - 1970;
    var month = date.getUTCMonth();
    var day = date.getUTCDate()-1;
    var hours = date.getUTCHours();
    var minutes = "0" + date.getUTCMinutes();
    var seconds = "0" + date.getUTCSeconds();
    var formattedTime = (year * 365 + month * 30 + day) + ' Ngày - ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    p.TimeLeft = formattedTime;

    var newDate= new Date(Date.now()-p.DateBegin);
    
    var year = newDate.getUTCFullYear() - 1970;
    var month = newDate.getUTCMonth();
    var day = newDate.getUTCDate()-1;
    var hours = newDate.getUTCHours();
    var minutes = newDate.getUTCMinutes();

    var checkNew= year * 365 *24*60 + month *30*24*60 + day*24*60 +hours*60 + minutes;
    
    if (checkNew<=10) p.isNew=true;
    else p.isNew=false;
    p.DateEnd = p.DateEnd.toLocaleDateString();
    p.DateBegin = p.DateBegin.toLocaleDateString();
};
router.get('/cat', async (req, res) => {
    res.redirect('/cat/1');
});
router.get('/cat/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    const page = parseInt(req.query.page) || 1;
    const cats = await mCat.all();
    const user= await mAccount.getByID(req.session.user);
    const ps = await mPro.allByCatIdPaging(id, page);
    const favorite=await favoriteM.ProIDByUserID(req.session.user);
    for (var i = 0; i < ps.products.length; i++) {
        
        CalTime(ps.products[i]);
        ps.products[i].formatedPrice=formatPrice(ps.products[i].CurrentPrice);
        ps.products[i].formatedInstPrice=formatPrice(ps.products[i].InstantPrice);
        ps.products[i].isFav=false;
        if (req.session.user){
            for (let fav of favorite){
                if (fav.ProID==ps.products[i].ProID)
                ps.products[i].isFav=true;
            }
       }
       var tbid =await mBid.topByProID(ps.products[i].ProID);
       if (tbid) {
        ps.products[i].TopName='***'+ tbid.f_Username.substring(3);
       }
        else ps.products[i].TopName='Chưa có';
    }
    var catName;
    for (let cat of cats) {
        cat.isActive = false;
        cat.subCat = [];
        if (cat.CatID === id) {
            cat.isActive = true;
            catName = cat.CatName;
        }
        const sub_cats = await mCat.allSubCatByID(cat.CatID);
        for (let sub_cat of sub_cats) {
            cat.subCat.push(sub_cat);
        }
    }
    const content = [
        {
            ps: ps.products,
            title: `DANH SÁCH SẢN PHẨM THUỘC ${catName} `
        },
    ];
    const pages = [];
    for (let i = 0; i < ps.pageTotal; i++) {
        pages[i] = { value: i + 1, active: (i + 1) === page };
    }
    const navs = {};
    if (page > 1) {
        navs.prev = page - 1;
    }
    if (page < ps.pageTotal) {
        navs.next = page + 1;
    }
    res.render('home', {
        title: 'Products',
        cats: cats,
        content: content,
        pages: pages,
        navs: navs,
        session:user,
    });

});

router.get('/subcat/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    const page = parseInt(req.query.page) || 1;
    const cats = await mCat.all();
    const ps = await mPro.allBySubCatIdPaging(id, page);
    const favorite=await favoriteM.ProIDByUserID(req.session.user);
    const user=await mAccount.getByID(req.session.user);
    for (var i = 0; i < ps.products.length; i++) {
        CalTime(ps.products[i]);
        ps.products[i].formatedPrice=formatPrice(ps.products[i].CurrentPrice);
        ps.products[i].formatedInstPrice=formatPrice(ps.products[i].InstantPrice)
        ps.products[i].isFav=false;
        if (req.session.user){
            for (let fav of favorite){
                if (fav.ProID==ps.products[i].ProID)
                ps.products[i].isFav=true;
            }
       }
       var tbid =await mBid.topByProID(ps.products[i].ProID);
       if (tbid) {
        ps.products[i].TopName='***'+ tbid.f_Username.substring(3);
       }
        else ps.products[i].TopName='Chưa có';
    }
    var catName,subCatName;
    for (let cat of cats) {
        cat.isActive = false;
        cat.subCat = [];
        const sub_cats = await mCat.allSubCatByID(cat.CatID);
        for (let sub_cat of sub_cats) {
            cat.subCat.push(sub_cat);
            if (sub_cat.SubCatID===id){
                cat.isActive = true;
                subCatName=sub_cat.SubCatName;
                catName = cat.CatName;
            }
        }
    }
    const content = [
        {
            ps: ps.products,
            title: `DANH SÁCH SẢN PHẨM THUỘC ${catName} - ${subCatName}`
        },
    ];
    const pages = [];
    for (let i = 0; i < ps.pageTotal; i++) {
        pages[i] = { value: i + 1, active: (i + 1) === page };
    }
    const navs = {};
    if (page > 1) {
        navs.prev = page - 1;
    }
    if (page < ps.pageTotal) {
        navs.next = page + 1;
    }
    res.render('home', {
        title: 'Products',
        cats: cats,
        content: content,
        pages: pages,
        navs: navs,
        session:user,
    });

});
module.exports = router;