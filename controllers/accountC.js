const express = require('express');
const router = express.Router();
const sha = require('sha.js');
const accountM = require('../models/accountM');
const favoriteM = require('../models/favoriteM');
var request = require('request');
const mrate = require('../models/ratingM');
const mBid = require('../models/bidM');
const mPermiss = require('../models/permissBidM');
const mPro=require('../models/productsM');
const hashLength = 64;
function formatPrice(price) {
    var result = " đ";
    while (price > 0) {
        let str = price.toString()
        let r = str.substring(str.length - 3, str.length);
        result = ',' + r + result;
        price = Math.floor(price / 1000);
    }
    return result.substring(1);
};
function CalTime(p) {
    var date = new Date(p.DateEnd - Date.now());

    var year = date.getUTCFullYear() - 1970;
    var month = date.getUTCMonth();
    var day = date.getUTCDate() - 1;
    var hours = date.getUTCHours();
    var minutes = "0" + date.getUTCMinutes();
    var seconds = "0" + date.getUTCSeconds();
    var formattedTime = (year * 365 + month * 30 + day) + ' Ngày - ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    p.TimeLeft = formattedTime;

    var newDate = new Date(Date.now() - p.DateBegin);

    var year = newDate.getUTCFullYear() - 1970;
    var month = newDate.getUTCMonth();
    var day = newDate.getUTCDate() - 1;
    var hours = newDate.getUTCHours();
    var minutes = newDate.getUTCMinutes();

    var checkNew = year * 365 * 24 * 60 + month * 30 * 24 * 60 + day * 24 * 60 + hours * 60 + minutes;

    if (checkNew <= 10) p.isNew = true;
    else p.isNew = false;
    p.DateEnd = p.DateEnd.toLocaleDateString();
    p.DateBegin = p.DateBegin.toLocaleDateString();
};
router.get('/account/logout', async (req, res) => {
    req.session.destroy();
    res.redirect('/');
});
router.get('/account/login', async (req, res) => {
    res.render('account/login', {
        isFail: false,
    });
});
router.get('/account/register', (req, res) => {
    res.render('account/register');
});
router.get('/account/profile', async (req, res) => {
    const session = await accountM.getByID(req.session.user);
    const user = await accountM.getByID(req.session.user);
    const bid = await mBid.allByUserID(req.session.user);
    const userRate = await mrate.RateByReceiveID(req.session.user);

    if (!user) {
        res.redirect('/account/login');
    }
    else {
        user.Rate = userRate;
        switch (user.f_Permission) {
            case 0:
                user.Type = 'Administrator';
                break;
            case 1:
                user.Type = 'Seller';
                break;
            case 2:
                user.Type = 'Bidder';
                break;
        }
        res.render('account/profile', {
            title: user.f_Username,
            session: session,
            user: user,
        });
    }

});

router.get('/account/:id/rating', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const rates = await mrate.allByReceiveID(id);
    const userRate = await mrate.RateByReceiveID(id);
    user.Rate = userRate;
    user.SellName = '***' + user.f_Username.substring(3);
    for (rate of rates) {
        rate.Date = rate.Date.toLocaleDateString();
        rate.SellName = '***' + rate.f_Username.substring(3);
    }
    res.render('account/rating', {
        title: user.f_Username,
        user: user,
        rates: rates,
        session: session,
    });
});
router.get('/tosell/:id/:status',async(req,res)=>{
    const session= await accountM.getByID(req.session.user);
    if(!session) 
    {
        res.redirect('/account/login');
        return;
    }
    const id=parseInt(req.params.id);
    const status=parseInt(req.params.status);
    const per={
        f_ID: id,
        f_Permission:2-status,
    }
        const uid= accountM.update(per);
        const did=mPermiss.delTosell(id);
    res.redirect(`/account/sellpermiss`);
});

router.get('/:id/tosell',async(req,res)=>{
    const id=parseInt(req.params.id);
    const session= await accountM.getByID(req.session.user);
    if(!session) 
    {
        res.redirect('/account/login');
        return;
    }
    const per={
        BidderID:id,
    }
    const exist= await mPermiss.isExistSell(id);
    if (exist) res.redirect('/account/profile');
    else {
        const aid=await mPermiss.addTosell(per);
    }
    
    res.redirect('/account/profile');
});

router.get('/account/sellpermiss', async (req, res) => {
    const user = await accountM.getByID(req.session.user);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const Permiss = await accountM.getByPermiss();
    for (Per of Permiss) {
        const userRate = await mrate.RateByReceiveID(Per.BidderID);
        Per.Date = Per.Date.toLocaleDateString();
        Per.SellName = '***' + Per.f_Username.substring(3);
        Per.Rate = userRate;
    }
    res.render('account/permiss', {
        title: user.f_Username,
        user: user,
        head: 'Danh sách xin nâng cấp tài khoản',
        Permiss: Permiss,
        session: session,
    });
});


router.get('/account/:id/permission', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const Permiss = await mPermiss.allPerBySellID(id);


    for (Per of Permiss) {
        const userRate = await mrate.RateByReceiveID(Per.BidderID);
        Per.Date = Per.Date.toLocaleDateString();
        Per.SellName = '***' + Per.f_Username.substring(3);
        Per.Rate = userRate;
    }
    res.render('account/permiss', {
        title: user.f_Username,
        user: user,
        head: 'Danh sách xin phép đấu giá',
        Permiss: Permiss,
        session: session,
    });
});
router.get('/account/:id/willsell', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const ps = await mPro.allSelBySelID(id);
    for (fa of ps) {
        CalTime(fa);
        fa.formatedPrice = formatPrice(fa.CurrentPrice);
        var top = await mBid.topByProID(fa.ProID);
        fa.isTop = false;
        if (top)
            if (fa.UserID == top.f_ID) fa.isTop = true;
    }
    res.render('account/bid', {
        title: user.f_Username,
        user: user,
        head:'Danh sách sản phẩm đã có người mua',
        ps: ps,
        session: session,
    });
});
router.get('/account/:id/selling', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const ps = await mPro.allBySelID(id);
    for (fa of ps) {
        CalTime(fa);
        fa.formatedPrice = formatPrice(fa.CurrentPrice);
        var top = await mBid.topByProID(fa.ProID);
        fa.isTop = false;
        if (top)
            if (fa.UserID == top.f_ID) fa.isTop = true;
    }
    res.render('account/bid', {
        title: user.f_Username,
        user: user,
        head:'Danh sách sản phẩm đang bán',
        ps: ps,
        session: session,
    });
});

router.get('/account/:id/bidding', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const ps = await mBid.allByUserID(id);
    for (fa of ps) {
        CalTime(fa);
        fa.formatedPrice = formatPrice(fa.CurrentPrice);
        var top = await mBid.topByProID(fa.ProID);
        fa.isTop = false;
        if (top)
            if (fa.UserID == top.f_ID) fa.isTop = true;
    }
    res.render('account/bid', {
        title: user.f_Username,
        user: user,
        head:'Danh sách sản phẩm đang tham gia đấu giá',
        ps: ps,
        session: session,
    });

});
router.get('/account/:id/won', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const ps = await mBid.allByUserID(id);
    for (fa of ps) {
        CalTime(fa);
        fa.formatedPrice = formatPrice(fa.CurrentPrice);
        var top = await mBid.topByProID(fa.ProID);
        var seller = await accountM.getByID(fa.SelID);
        fa.isTop = false;
        fa.Seller = '***' + seller.f_Username.substring(3);;
        if (top)
            if (fa.UserID == top.f_ID) fa.isTop = true;
    }
    res.render('account/won', {
        title: user.f_Username,
        user: user,
        ps: ps,
        session: session,
    });

});
router.get('/account/:id/favorite', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    const fav = await favoriteM.allProByUserID(id);
    for (fa of fav) {
        CalTime(fa);
        fa.formatedPrice = formatPrice(fa.CurrentPrice);
    }
    res.render('account/favorite', {
        title: user.f_Username,
        user: user,
        fav: fav,
        session: session,
    });

});
router.get('/rate/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    const user = await accountM.getByID(id);
    const session = await accountM.getByID(req.session.user);
    if (!session) {
        res.redirect('/account/login');
        return;
    }
    res.render('account/rate', {
        title: user.f_Username,
       
        reUser: user,
        SeUser: session,
        session: session,
    });
});
router.post('/rate/:id', async (req, res) => {
    const content = req.body.comment;
    const rate = parseInt(req.body.rate);
    const id = parseInt(req.params.id);
    const rating = {
        SendID: req.session.user,
        ReceiveID: id,
        Rate: rate,
        Content: content,
    };
    const uId = await mrate.add(rating);
    res.redirect('/');
});
router.post('/account/favorite', async (req, res) => {
    const proId = parseInt(req.body.pro);
    const userId = parseInt(req.body.user);
    const user = await accountM.getByID(proId);
    const favorite = {
        ProID: proId,
        UserID: userId,
    };

    const Exist = await favoriteM.isExist(proId, userId);
    if (Exist.exist == 1) {
        const uId = await favoriteM.del(proId, userId);
    }
    else {
        const uId = await favoriteM.add(favorite);
    }
    res.send(`${Exist.exist}`);
});
router.post('/account/login', async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const user = await accountM.getByUsername(username);
    if (user === null) {
        res.render('account/login', {
            isFail: true,
        });
        return;
    }
    const pwDb = user.f_Password;
    const salt = pwDb.substring(hashLength, pwDb.length);
    const preHash = password + salt;
    const hash = sha('sha256').update(preHash).digest('hex');
    const pwHash = hash + salt;

    if (pwHash === pwDb) {
        req.session.user = user.f_ID;
        res.redirect('/');
    }
    else res.render('account/login', {
        isFail: true,
    });
});
router.post('/account/edit', async (req, res) => {
    const getUser = await accountM.getByID(req.body.UserID);
    var userID = getUser.f_ID;
    var username = getUser.f_Username || req.body.username;
    var name = getUser.f_Name || req.body.name;
    var email = getUser.f_Email || req.body.email;
    var address = getUser.f_Address || req.body.address;
    var password = getUser.f_Password;
    const uemail = await accountM.getByEmail(email);
    if (uemail != null) {
        res.redirect('/account/profile');
        return;
    }
    if (req.body.newpassword) {
        const salt = Date.now().toString(16);
        const preHash = req.body.newpassword + salt;
        const hash = sha('sha256').update(preHash).digest('hex');
        password = hash + salt;
    }
    var user = {
        f_ID: req.body.UserID,
        f_Username: username,
        f_Name: name,
        f_Email: email,
        f_Address: address,
        f_Password: password,
    };
    const uId = await accountM.update(user);
    res.redirect('/account/profile');
});
router.post('/account/register', async (req, res) => {
    // g-recaptcha-response is the key that browser will generate upon form submit.
    // if its blank or null means user has not selected the captcha, so return the error.
    if (req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
        return res.json({ "responseCode": 1, "responseDesc": "Please select captcha" });
    }
    // Put your secret key here.
    var secretKey = "6LcPm8sUAAAAAD1E3O7L1pk6y4NsV3XZKPhmXjJw";
    // req.connection.remoteAddress will provide IP address of connected user.
    var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
    // Hitting GET request to the URL, Google will respond with success or error scenario.
    request(verificationUrl, async function (error, response, body) {
        body = JSON.parse(body);
        // Success will be true or false depending upon captcha validation.
        if (body.success !== undefined && !body.success) {

            return res.render('account/register');
        }
        const username = req.body.username;
        const password = req.body.password;
        const email = req.body.email;
        const uemail = await accountM.getByEmail(email);
        if (uemail != null) {
            res.render('account/register', {
                isFail: true,
            });
            return;
        }
        const salt = Date.now().toString(16);
        const preHash = password + salt;
        const hash = sha('sha256').update(preHash).digest('hex');
        const pwHash = hash + salt;
        const user = {
            f_Username: username,
            f_Password: pwHash,
            f_Name: req.body.name,
            f_Email: req.body.email,
            f_Address: req.body.address,
            f_Permission: 2,
        };
        const uId = await accountM.add(user);
        res.redirect('/account/login');
    });

});
module.exports = router;