const express = require('express');
const router = express.Router();
const mPro = require('../models/productsM');
const mCat = require('../models/categoryM')
const mAccount = require('../models/accountM');
const mBid = require('../models/bidM');
const favoriteM = require('../models/favoriteM');
const mrate = require('../models/ratingM');
const axios = require('axios');
const mPer=require('../models/permissBidM');
function formatPrice(price) {
    var result = " đ";
    while (price > 0) {
        let str = price.toString()
        let r = str.substring(str.length - 3, str.length);
        result = ',' + r + result;
        price = Math.floor(price / 1000);
    }
    return result.substring(1);
};
function CalTime(p) {
    var date = new Date(p.DateEnd - Date.now());

    var year = date.getUTCFullYear() - 1970;
    var month = date.getUTCMonth();
    var day = date.getUTCDate() - 1;
    var hours = date.getUTCHours();
    var minutes = "0" + date.getUTCMinutes();
    var seconds = "0" + date.getUTCSeconds();
    var formattedTime = (year * 365 + month * 30 + day) + ' Ngày - ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    p.TimeLeft = formattedTime;

    var newDate = new Date(Date.now() - p.DateBegin);

    var year = newDate.getUTCFullYear() - 1970;
    var month = newDate.getUTCMonth();
    var day = newDate.getUTCDate() - 1;
    var hours = newDate.getUTCHours();
    var minutes = newDate.getUTCMinutes();

    var checkNew = year * 365 * 24 * 60 + month * 30 * 24 * 60 + day * 24 * 60 + hours * 60 + minutes;

    if (checkNew <= 10) p.isNew = true;
    else p.isNew = false;
    p.DateEnd = p.DateEnd.toLocaleDateString();
    p.DateBegin = p.DateBegin.toLocaleDateString();
};
router.get('/upload/:id',async(req,res)=>{
    const cats = await mCat.all();
    const user = await mAccount.getByID(req.session.user);
    const session = await mAccount.getByID(req.session.user);
    if (!user) {
        res.redirect('/account/login');
    }
    for (let cat of cats) {
        cat.isActive = false;
        cat.subCat = [];
        const sub_cats = await mCat.allSubCatByID(cat.CatID);
        for (let sub_cat of sub_cats) {
            cat.subCat.push(sub_cat);
        }
    }
    res.render('products/upload',{
        title: 'Products',
        cat:cats,
        session:session,
    });
});
router.get('/search', async (req, res) => {
    const key = req.query.key;
    const select = req.query.SearchSelect;
    const page = parseInt(req.query.page) || 1;
    const order = req.query.order || 0;
    const user = await mAccount.getByID(req.session.user);
    const cats = await mCat.all();
    const ps_name = await mPro.search('ProName', key, order, page);
    const ps_cat = await mPro.searchByCat('CatName', key, order, page);
    const favorite = await favoriteM.ProIDByUserID(req.session.user);
    var ps = ps_name;
    if (select == '2') {
        ps = ps_cat;
    }
    for (var i = 0; i < ps.products.length; i++) {
        CalTime(ps.products[i]);
        ps.products[i].formatedPrice = formatPrice(ps.products[i].CurrentPrice);
        ps.products[i].formatedInstPrice = formatPrice(ps.products[i].InstantPrice);
        ps.products[i].isFav = false;

        if (req.session.user) {
            for (let fav of favorite) {
                if (fav.ProID == ps.products[i].ProID)
                    ps.products[i].isFav = true;
            }
        }
        var tbid = await mBid.topByProID(ps.products[i].ProID);
        if (tbid) {
            ps.products[i].TopName = tbid.f_Username;
        }
        else ps.products[i].TopName = 'Chưa có';
    }
    for (let cat of cats) {
        cat.isActive = false;
        cat.subCat = [];
        const sub_cats = await mCat.allSubCatByID(cat.CatID);
        for (let sub_cat of sub_cats) {
            cat.subCat.push(sub_cat);
        }
    }
    const content = [
        {
            ps: ps.products,
            title: `KẾT QUẢ TÌM KIẾM TỪ KHÓA ${key}`
        },
    ];
    const pages = [];
    for (let i = 0; i < ps.pageTotal; i++) {
        pages[i] = { value: i + 1, active: (i + 1) === page };
    }
    const navs = {};
    if (page > 1) {
        navs.prev = page - 1;
    }
    if (page < ps.pageTotal) {
        navs.next = page + 1;
    }
    res.render('home', {
        title: 'Products',
        cats: cats,
        content: content,
        pages: pages,
        navs: navs,
        key: key,
        select: select,
        session: user,
        page: page,
        isOrder1: order == 1,
        isOrder2: order == 2,
        order: order,
    });

});

router.get('/product/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    const ps = await mPro.allByProID(id);
    const user = await mAccount.getByID(req.session.user);
    const rate = await mrate.RateByReceiveID(ps[0].SelID);
    const srate = await mrate.RateByReceiveID(req.session.user);
    const seller = await mAccount.getByID(ps[0].SelID);
    const bid = await mBid.allByProID(id);
    const favorite = await favoriteM.ProIDByUserID(req.session.user);
    const per=await mPer.getPermissByBidID(id,req.session.user)
    if (user)
        user.isRate = (srate >= 80 || per==1);
    for (let p of ps) {
        function CalReativeTime(p) {
            var date = new Date(p.DateEnd - Date.now());
            var year = date.getUTCFullYear() - 1970;
            var month = date.getUTCMonth();
            var day = date.getUTCDate() - 1;
            var hours = date.getUTCHours();
            var minutes = "0" + date.getUTCMinutes();
            var seconds = "0" + date.getUTCSeconds();
            var dayNum = (year * 365 + month * 30 + day);
            var formattedTime = "";
            if (dayNum < 3) {
                if (dayNum > 0) 
                    formattedTime = 'Còn ' + dayNum + ' ngày nữa';
                if (dayNum == 0)
                {
                    if (hours > 0) formattedTime = 'Còn ' + hours + ' giờ nữa';
                        if (hours == 0)
                            if (minutes > 0) formattedTime = 'Còn ' + minutes.substr(-2) + ' phút nữa';
                }
            }
            if (dayNum >= 3) formattedTime = (year * 365 + month * 30 + day) + ' Ngày - ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
            p.TimeLeft = formattedTime;
            var newDate = new Date(Date.now() - p.DateBegin);
            var year = newDate.getUTCFullYear() - 1970;
            var month = newDate.getUTCMonth();
            var day = newDate.getUTCDate() - 1;
            var hours = newDate.getUTCHours();
            var minutes = newDate.getUTCMinutes();
            var checkNew = year * 365 * 24 * 60 + month * 30 * 24 * 60 + day * 24 * 60 + hours * 60 + minutes;
            if (checkNew <= 10) p.isNew = true;
            else p.isNew = false;
            p.DateEnd = p.DateEnd.toLocaleDateString();
            p.DateBegin = p.DateBegin.toLocaleDateString();
        };
        CalReativeTime(p);
        p.formatedPrice = formatPrice(p.CurrentPrice);
        p.formatedInstPrice = formatPrice(p.InstantPrice)
        p.isFav = false;
        p.SellName = '***' + seller.f_Username.substring(3);
        p.SellRate = rate;
        if (req.session.user) {
            for (let fav of favorite) {
                if (fav.ProID == p.ProID)
                    p.isFav = true;
            }
        }
    }
    for (let b of bid) {
        b.DateBegin = b.DateBegin.toLocaleString("en-US");
        b.formatedPrice = formatPrice(b.Price); 
        b.f_Username = '***' + b.f_Username.substring(3);
        const brate = await mrate.RateByReceiveID(b.f_ID);
        b.Rate = brate;
    }
    var own = user;
    if (own) own = (user.f_ID == ps[0].SelID);
    else own = false;
    var isBidder=false;
    var notOwn=true;
    if(user) isBidder = user.f_Permission==2;
    if(user) notOwn =!(user.f_Permission==1 || user.f_Permission==0);
    res.render('products/detail', {
        title: `Product ${ps[0].ProName}`,
        ps: ps[0],
        his: bid,
        session: user,
        own: own,
        notOwn: notOwn,
        isBidder: isBidder,
    });
});
router.post('/upload',async(req,res)=>{
    const user = await mAccount.getByID(req.session.user);
    const pro=await mPro.getID();
    const catId=await mCat.CatBySubCatID(req.body.subcat);
    let hinh1 = req.files.hinh1;
    let hinh2 = req.files.hinh2;
    let hinh3 = req.files.hinh3;
    let delay=req.body.delay;
    if (delay=='') delay=0;
    else delay=1;
    const apro={
        ProName:req.body.name,
        FullDes:req.body.des,
        Price:req.body.price,
        CurrentPrice: req.body.price,
        InstantPrice: (req.body.insprice!='') ? req.body.insprice : 0,
        SubCatID: req.body.subcat,
        CatID:catId,
        Step:req.body.step,
        AutoDelay:delay,
        DateEnd: req.body.enddate +' '+ req.body.endtime + ':00',
        SelID:user.f_ID,
    };
    if (!user) {
        res.redirect('/account/login');
    }
    const aid = await mPro.add(apro);
    hinh1.mv(`./public/db/sp/${pro}/1.jpg`, function(err) {
        if (err)
          return res.status(500).send(err);
    });
    hinh1.mv(`./public/db/sp/${pro}/main.jpg`, function(err) {
        if (err)
          return res.status(500).send(err);
    });
    hinh1.mv(`./public/db/sp/${pro}/main_thumbs.jpg`, function(err) {
        if (err)
          return res.status(500).send(err);
    });
    hinh2.mv(`./public/db/sp/${pro}/2.jpg`, function(err) {
        if (err)
          return res.status(500).send(err);
    });
    hinh3.mv(`./public/db/sp/${pro}/3.jpg`, function(err) {
        if (err)
          return res.status(500).send(err);
    });
    res.redirect('/');
});
module.exports = router;