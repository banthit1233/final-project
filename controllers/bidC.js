const express = require('express');
const router = express.Router();
const mPro = require('../models/productsM');
const mCat = require('../models/categoryM')
const mAccount=require('../models/accountM');
const mBid=require('../models/bidM');
const mPermiss=require('../models/permissBidM');
router.post('/bid', async (req, res) => {
    const proId= parseInt(req.body.pro);
    const userId= parseInt(req.body.user);
    const price=parseInt(req.body.price);
    const ps=await mPro.allByProID(proId);
    const bid={
        ProID:proId,
        UserID:userId,
        Price:price,
    }
    const pro={
        ProID: proId,
        CurrentPrice:price,
        Amount: ps[0].Amount+1,
    };
    const abid= await mBid.add(bid);
    const upro=await mPro.update(pro);
    res.redirect(`product/${proId}`);
});
router.get('/permiss/:id/:status',async(req,res)=>{
    const session= await mAccount.getByID(req.session.user);
    if(!session) 
    {
        res.redirect('/account/login');
        return;
    }
    const id=parseInt(req.params.id);
    const status=parseInt(req.params.status);
    const per={
        PermissID:id,
        Status:status,
    }
    if (status==1) 
    {
        const uid=mPermiss.update(per); 
    }
    else if (status==0)
    {
        const did=mPermiss.del(id);
    }
    res.redirect(`/account/${session.f_ID}/permission`);
})
router.post('/permiss',async(req,res)=>{
    const proId= parseInt(req.body.Pro);
    const SelID= parseInt(req.body.Sel);
    const BidID=parseInt(req.body.Bid);
    const permiss={
        BidderID:BidID,
        SellerID:SelID,
        ProID:proId
    }
    const iper=await mPermiss.isExist(proId,BidID,SelID);
    if (!iper)
    {
        const aper=await mPermiss.add(permiss);
    }
    
    res.redirect(`product/${proId}`);
});
module.exports = router;