const express = require('express');
const router = express.Router();
const session = require('express-session');
const mPro = require('../models/productsM');
const mCat = require('../models/categoryM');
const mAcc = require('../models/accountM');
const favoriteM = require('../models/favoriteM');
const mBid = require('../models/bidM');
function formatPrice(price) {
    var result = " đ";
    while (price > 0) {
        let str = price.toString()
        let r = str.substring(str.length - 3, str.length);
        result = ',' + r + result;
        price = Math.floor(price / 1000);
    }
    return result.substring(1);
}
function CalTime(p) {
    var date = new Date(p.DateEnd - Date.now());
    
    var year = date.getUTCFullYear() - 1970;
    var month = date.getUTCMonth();
    var day = date.getUTCDate()-1;
    var hours = date.getUTCHours();
    var minutes = "0" + date.getUTCMinutes();
    var seconds = "0" + date.getUTCSeconds();
    var formattedTime = (year * 365 + month * 30 + day) + ' Ngày - ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    p.TimeLeft = formattedTime;

    var newDate= new Date(Date.now()-p.DateBegin);
    
    var year = newDate.getUTCFullYear() - 1970;
    var month = newDate.getUTCMonth();
    var day = newDate.getUTCDate()-1;
    var hours = newDate.getUTCHours();
    var minutes = newDate.getUTCMinutes();

    var checkNew= year * 365 *24*60 + month *30*24*60 + day*24*60 +hours*60 + minutes;
    
    if (checkNew<=10) p.isNew=true;
    else p.isNew=false;
    p.DateEnd = p.DateEnd.toLocaleDateString();
    p.DateBegin = p.DateBegin.toLocaleDateString();
};
router.get('/', async (req, res) => {
    const ps_time = await mPro.allByOrder('TimeLeft', false);
    const ps_amount = await mPro.allByOrder('Amount', true);
    const ps_price = await mPro.allByOrder('Price', true);
    const cats = await mCat.all();
    const user = await mAcc.getByID(req.session.user);
    const favorite = await favoriteM.ProIDByUserID(req.session.user);

    for (var i = 0; i < ps_time.length; i++) {
       
        CalTime(ps_time[i]);
        CalTime(ps_amount[i]);
        CalTime(ps_price[i]);

        if (ps_time[i].InstantPrice) ps_time[i].formatedInstPrice = formatPrice(ps_time[i].InstantPrice);
        if (ps_amount[i].InstantPrice) ps_amount[i].formatedInstPrice = formatPrice(ps_amount[i].InstantPrice);
        if (ps_price[i].InstantPrice) ps_price[i].formatedInstPrice = formatPrice(ps_price[i].InstantPrice);


        ps_time[i].formatedPrice = formatPrice(ps_time[i].CurrentPrice);
        ps_amount[i].formatedPrice = formatPrice(ps_amount[i].CurrentPrice);
        ps_price[i].formatedPrice = formatPrice(ps_price[i].CurrentPrice);

        ps_time[i].isFav = false;
        ps_amount[i].isFav = false;
        ps_price[i].isFav = false;
        if (req.session.user) {
            for (let fav of favorite) {
                if (fav.ProID == ps_time[i].ProID)
                    ps_time[i].isFav = true;
                if (fav.ProID == ps_amount[i].ProID)
                    ps_amount[i].isFav = true;
                if (fav.ProID == ps_price[i].ProID)
                    ps_price[i].isFav = true;
            }
        }
        

        var tbid = await mBid.topByProID(ps_time[i].ProID);
        if (tbid) {
            ps_time[i].TopName = '***'+ tbid.f_Username.substring(3);
        }
        else ps_time[i].TopName = 'Chưa có';

        tbid = await mBid.topByProID(ps_amount[i].ProID);
        if (tbid) {
            ps_amount[i].TopName = tbid.f_Username;
        }
        else ps_price[i].TopName = 'Chưa có';
        tbid = await mBid.topByProID(ps_price[i].ProID);
        if (tbid) {
            ps_price[i].TopName = tbid.f_Username;
        }
        else ps_price[i].TopName = 'Chưa có';
    }
    ps_price.title = 'TOP 5 SẢN PHẨM CÓ GIÁ CAO NHẤT';
    const content = [
        {
            ps: ps_time,
            title: 'TOP 5 SẢN PHẨM GẦN KẾT THÚC ĐẤU GIÁ '
        },

        {
            ps: ps_amount,
            title: 'TOP 5 SẢN PHẨM CÓ NHIỀU LƯỢT RA GIÁ NHẤT'
        },

        {
            ps: ps_price,
            title: 'TOP 5 SẢN PHẨM CÓ GIÁ CAO NHẤT'
        }];
    for (let cat of cats) {
        cat.isActive = false;
        cat.subCat = [];
        const sub_cats = await mCat.allSubCatByID(cat.CatID);
        for (let sub_cat of sub_cats) {
            cat.subCat.push(sub_cat);
        }
    }
    res.render('home', {
        title: 'Trang chủ',
        content: content,
        cats: cats,
        session: user,
    });
});

module.exports = router;