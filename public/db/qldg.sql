-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: qldg
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bid_history`
--

DROP TABLE IF EXISTS `bid_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bid_history` (
  `BidID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProID` int(11) unsigned NOT NULL,
  `UserID` int(11) NOT NULL,
  `DateBegin` datetime DEFAULT NULL,
  PRIMARY KEY (`BidID`),
  KEY `ProID` (`ProID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bid_history`
--

LOCK TABLES `bid_history` WRITE;
/*!40000 ALTER TABLE `bid_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `bid_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `CatID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CatName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`CatID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Thiết Bị Điện Tử'),(2,'Điện Gia Dụng'),(3,'Làm Đẹp - Sức Khỏe'),(4,'Thời Trang - Phụ Kiện'),(5,'Nhà cửa');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorite`
--

DROP TABLE IF EXISTS `favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favorite` (
  `ProID` int(11) unsigned NOT NULL,
  `UserID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ProID`,`UserID`),
  KEY `UserID` (`UserID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorite`
--

LOCK TABLES `favorite` WRITE;
/*!40000 ALTER TABLE `favorite` DISABLE KEYS */;
INSERT INTO `favorite` VALUES (1,1),(2,1),(4,1),(5,1);
/*!40000 ALTER TABLE `favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `ProID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `FullDes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Price` int(11) NOT NULL,
  `CurrentPrice` int(11) DEFAULT NULL,
  `InstantPrice` int(11) DEFAULT NULL,
  `CatID` int(11) NOT NULL,
  `AutoDelay` tinyint(1) NOT NULL,
  `SubCatID` int(11) NOT NULL,
  `Amount` int(11) DEFAULT '0',
  `DateEnd` datetime DEFAULT NULL,
  `SelID` int(11) DEFAULT NULL,
  `DateBegin` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Điện Thoại Samsung Galaxy M10','<UL><LI>Điện thoại chính hãng, Nguyên seal, Mới 100%, Chưa Active</LI><LI>Thiết kế: Nguyên khối, mặt kính cong 2.5D</LI><LI>Màn hình: LCD 6.2\" HD+, Infinity V Display</LI><LI>Camera Sau: 13 MP và 5 MP (2 camera)</LI><LI>Camera Trước: 5 MP</LI><LI>CPU: Samsung Exynos 7870, 8 nhân Cortex A53 @1.6 GHz</LI><LI>Bộ Nhớ: 16GB</LI><LI>RAM: 2GB</LI><LI>Tính năng: Mở khóa bằng vân tay, Đèn pin, Chặn cuộc gọi, Chặn tin nhắn</LI></UL>',2500000,2500000,2990000,1,1,1,0,'2020-01-04 00:10:00',1,'2019-12-31 22:57:36'),(2,'Điện Thoại Realme 2 Pro','<UL><LI>Chính hãng, Nguyên seal, Mới 100%</LI><LI>Miễn phí giao hàng toàn quốc</LI><LI>Thiết kế nguyên khối</LI><LI>Màn hình: 6.3 inch, Full HD+ (1080 x 2340 Pixels)</LI><LI>Camera Trước / Sau: 16MP / Camera kép 16MP + 2MP</LI><LI>CPU: Qualcomm Snapdragon 660 8 nhân</LI><LI>Bộ Nhớ: 64GB</LI><LI>RAM: 4GB</LI><LI>SIM tương thích: Nano SIM</LI><LI>Tính năng: Mở khóa bằng vân tay, Mở khóa bằng khuôn mặt</LI></UL>',3400000,3400000,4000000,1,0,1,0,'2020-01-03 10:10:00',1,'2019-12-31 22:57:36'),(3,'Điện Thoại Xiaomi Mi 9','<UL><LI>Xiaomi Mi 9 Lite là phiên bản quốc tế của chiếc Mi CC9 ra mắt trong tháng 7/2019</LI><LI>Mới, Chính hãng, Nguyên seal, Chưa active</LI><LI>Miễn phí giao hàng toàn quốc</LI><LI>Màn hình: Super AMOLED, 6.39\", Full HD+</LI><LI>Hệ điều hành: Android 9.0 (Pie)</LI><LI>Camera sau: Chính 48 MP & Phụ 8 MP, 2 MP</LI><LI>Camera trước: 32 MP</LI><LI>CPU: Snapdragon 710 8 nhân 64-bit</LI><LI>RAM: 6 GB</LI><LI>Bộ nhớ trong: 64GB</LI><LI>Thẻ nhớ: MicroSD, hỗ trợ tối đa 256 GB</LI><LI>Thẻ SIM: 2 SIM Nano (SIM 2 chung khe thẻ nhớ)</LI><LI>Dung lượng pin: 4030 mAh</LI></UL>',5000000,5000000,5600000,1,0,1,0,'2020-03-01 00:09:00',1,'2019-12-31 22:57:36'),(4,'Điện Thoại OPPO F11 ','<UL><LI>Chính hãng, Nguyên seal, Mới 100%, Chưa Active</LI><LI>Miễn phí giao hàng toàn quốc</LI><LI>Màn hình: LTPS LCD, 6.53\", Full HD+ (1080 x 2340 Pixels)</LI><LI>Camera Trước/Sau: 16MP/48MP + 5MP (2 camera)</LI><LI>CPU: MediaTek Helio P70 8 nhân</LI><LI>Bộ Nhớ: 64GB</LI><LI>RAM: 6GB</LI><LI>SIM tương thích: 2 Nano SIM</LI><LI>Tính năng: Mở khóa bằng vân tay, Mở khóa bằng khuôn mặt</LI></UL>',6000000,6000000,7000000,1,1,1,0,'2020-02-01 00:10:00',1,'2019-12-31 22:57:36'),(5,'Điện Thoại iPhone XS 256GB','<UL><LI>Mã Quốc Tế: LL/ZA/ZP/..</LI><LI>Nguyên seal, mới 100%, chưa active</LI><LI>Miễn phí giao hàng toàn quốc</LI><LI>Thiết kế: Nguyên khối</LI><LI>Màn hình OLED: 5.8 inch Super Retina (2436 x 1125), 463ppi, 3D Touch, TrueTone Dolby Vision HDR 10</LI><LI>Camera Trước/Sau: 7MP/ 2 camera 12MP</LI><LI>CPU: A12 Bionic 64-bit 7nm</LI><LI>Bộ Nhớ: 256GB</LI><LI>RAM: 4GB</LI><LI>SIM: 1 Nano SIM, 1 eSIM</LI><LI>Đạt chuẩn chống nước bụi IP68, Face ID</LI></UL>',20000000,20000000,30000000,1,0,1,0,'2020-01-05 00:10:00',1,'2019-12-31 22:57:36');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_categories` (
  `SubCatID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SubCatName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CatID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`SubCatID`) USING BTREE,
  KEY `CatID` (`CatID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_categories`
--

LOCK TABLES `sub_categories` WRITE;
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` VALUES (1,'Điện Thoại',1),(2,'Máy Tính Xách Tay',1),(3,'Máy Ảnh - Quay Phim',1),(4,'Đồ Dùng Nhà Bếp',2),(5,'Thiết Bị Gia Đình',2),(6,'Trang Điểm',3),(7,'Chăm Sóc Cơ Thể',3),(8,'Quần Áo',4),(9,'Giày Dép',4),(10,'Trang Trí Nhà Cửa',5),(11,'Nội Thất',5);
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `f_ID` int(11) NOT NULL AUTO_INCREMENT,
  `f_Username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `f_Password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `f_Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `f_Email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `f_DOB` date NOT NULL,
  `f_Permission` int(11) NOT NULL,
  PRIMARY KEY (`f_ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'123','2bd85cb698c0c6f816b3c7e39dcbb1ce6e9e9f7423766abffaf9c79badbf85ad16f0226921e','123','123@123','1998-09-18',1),(2,'123','5d238863670951c94376d8c79d1ef5a9b5a5e0b73c3914a77586f0bc14d84a9216f02271aa7','123','123@123','1998-09-18',0),(3,'123','158ab061572706592804a2f4dc45bf56383dca1d23dd695ff89d2578731f20cd16f0227a153','123','123@123','1998-09-18',0),(4,'333','2f0e5b6eee70018e39e4ffb1e14d650ceb6caf78d936b5671311962a36470f7716f022876c1','123','123@123','2019-12-14',0),(5,'lalalalla','200720ee7da976ead938ccd0709e8ad5490d2dc1c404aec4f0ab8474c50768ac16f02296c67','1234','123@123','2010-10-20',0),(6,'bidder','d27dfee969dd2e1216c728c3d0524fe49491c66ba29e0c7ca0207d5bf7dbfe1516f5ce91fca','bidder','nhluan.12tin@gmail.com','1998-09-18',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'qldg'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-01  4:26:14
